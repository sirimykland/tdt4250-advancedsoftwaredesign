/**
 */
package studyprogram;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialisation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link studyprogram.Specialisation#getName <em>Name</em>}</li>
 *   <li>{@link studyprogram.Specialisation#getSemesterList <em>Semester List</em>}</li>
 *   <li>{@link studyprogram.Specialisation#getSpecialisations <em>Specialisations</em>}</li>
 * </ul>
 *
 * @see studyprogram.StudyprogramPackage#getSpecialisation()
 * @model
 * @generated
 */
public interface Specialisation extends Semester {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see studyprogram.StudyprogramPackage#getSpecialisation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link studyprogram.Specialisation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Semester List</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Semester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester List</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getSpecialisation_SemesterList()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semester> getSemesterList();

	/**
	 * Returns the value of the '<em><b>Specialisations</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Specialisation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisations</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getSpecialisation_Specialisations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Specialisation> getSpecialisations();

} // Specialisation
