/**
 */
package studyprogram.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import studyprogram.Semester;
import studyprogram.SemesterCourse;
import studyprogram.SemesterSeason;
import studyprogram.StudyprogramPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link studyprogram.impl.SemesterImpl#getSemesterNumber <em>Semester Number</em>}</li>
 *   <li>{@link studyprogram.impl.SemesterImpl#getSemesterCourses <em>Semester Courses</em>}</li>
 *   <li>{@link studyprogram.impl.SemesterImpl#getSemesterSeason <em>Semester Season</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The default value of the '{@link #getSemesterNumber() <em>Semester Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMESTER_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSemesterNumber() <em>Semester Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterNumber()
	 * @generated
	 * @ordered
	 */
	protected String semesterNumber = SEMESTER_NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSemesterCourses() <em>Semester Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<SemesterCourse> semesterCourses;

	/**
	 * The default value of the '{@link #getSemesterSeason() <em>Semester Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterSeason()
	 * @generated
	 * @ordered
	 */
	protected static final SemesterSeason SEMESTER_SEASON_EDEFAULT = SemesterSeason.FALL;

	/**
	 * The cached value of the '{@link #getSemesterSeason() <em>Semester Season</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemesterSeason()
	 * @generated
	 * @ordered
	 */
	protected SemesterSeason semesterSeason = SEMESTER_SEASON_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StudyprogramPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSemesterNumber() {
		return semesterNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemesterNumber(String newSemesterNumber) {
		String oldSemesterNumber = semesterNumber;
		semesterNumber = newSemesterNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyprogramPackage.SEMESTER__SEMESTER_NUMBER, oldSemesterNumber, semesterNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SemesterCourse> getSemesterCourses() {
		if (semesterCourses == null) {
			semesterCourses = new EObjectContainmentEList<SemesterCourse>(SemesterCourse.class, this, StudyprogramPackage.SEMESTER__SEMESTER_COURSES);
		}
		return semesterCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SemesterSeason getSemesterSeason() {
		return semesterSeason;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSemesterSeason(SemesterSeason newSemesterSeason) {
		SemesterSeason oldSemesterSeason = semesterSeason;
		semesterSeason = newSemesterSeason == null ? SEMESTER_SEASON_EDEFAULT : newSemesterSeason;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StudyprogramPackage.SEMESTER__SEMESTER_SEASON, oldSemesterSeason, semesterSeason));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StudyprogramPackage.SEMESTER__SEMESTER_COURSES:
				return ((InternalEList<?>)getSemesterCourses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StudyprogramPackage.SEMESTER__SEMESTER_NUMBER:
				return getSemesterNumber();
			case StudyprogramPackage.SEMESTER__SEMESTER_COURSES:
				return getSemesterCourses();
			case StudyprogramPackage.SEMESTER__SEMESTER_SEASON:
				return getSemesterSeason();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StudyprogramPackage.SEMESTER__SEMESTER_NUMBER:
				setSemesterNumber((String)newValue);
				return;
			case StudyprogramPackage.SEMESTER__SEMESTER_COURSES:
				getSemesterCourses().clear();
				getSemesterCourses().addAll((Collection<? extends SemesterCourse>)newValue);
				return;
			case StudyprogramPackage.SEMESTER__SEMESTER_SEASON:
				setSemesterSeason((SemesterSeason)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StudyprogramPackage.SEMESTER__SEMESTER_NUMBER:
				setSemesterNumber(SEMESTER_NUMBER_EDEFAULT);
				return;
			case StudyprogramPackage.SEMESTER__SEMESTER_COURSES:
				getSemesterCourses().clear();
				return;
			case StudyprogramPackage.SEMESTER__SEMESTER_SEASON:
				setSemesterSeason(SEMESTER_SEASON_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StudyprogramPackage.SEMESTER__SEMESTER_NUMBER:
				return SEMESTER_NUMBER_EDEFAULT == null ? semesterNumber != null : !SEMESTER_NUMBER_EDEFAULT.equals(semesterNumber);
			case StudyprogramPackage.SEMESTER__SEMESTER_COURSES:
				return semesterCourses != null && !semesterCourses.isEmpty();
			case StudyprogramPackage.SEMESTER__SEMESTER_SEASON:
				return semesterSeason != SEMESTER_SEASON_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semesterNumber: ");
		result.append(semesterNumber);
		result.append(", semesterSeason: ");
		result.append(semesterSeason);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
