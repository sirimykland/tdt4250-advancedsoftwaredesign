/**
 */
package studyprogram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Institute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link studyprogram.Institute#getInstituteName <em>Institute Name</em>}</li>
 *   <li>{@link studyprogram.Institute#getCourses <em>Courses</em>}</li>
 *   <li>{@link studyprogram.Institute#getPrograms <em>Programs</em>}</li>
 * </ul>
 *
 * @see studyprogram.StudyprogramPackage#getInstitute()
 * @model
 * @generated
 */
public interface Institute extends EObject {
	/**
	 * Returns the value of the '<em><b>Institute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institute Name</em>' attribute.
	 * @see #setInstituteName(String)
	 * @see studyprogram.StudyprogramPackage#getInstitute_InstituteName()
	 * @model
	 * @generated
	 */
	String getInstituteName();

	/**
	 * Sets the value of the '{@link studyprogram.Institute#getInstituteName <em>Institute Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Institute Name</em>' attribute.
	 * @see #getInstituteName()
	 * @generated
	 */
	void setInstituteName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getInstitute_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Programs</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Program}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Programs</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getInstitute_Programs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Program> getPrograms();

} // Institute
