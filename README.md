## Assignment 3 - M2T using Acceleo

The improved Ecore model from assignment 1 can be found in tdt4250.assignment3.model/model

The the output html file can be found in tdt4250.assignment3.html/html (not a link)

The package tdt4250.assignment3.html/src/...main (not a link) contains both:

1. The Acceleo module studyprogram2TextGenerator and 

2. the generated the java class Studyprogram2TextGenerator, 



## Assignment 1
can be found in the ModelingAssignment1/ and the ModelingAssignment1.tests/ 

1. inside the  ModelingAssignment1/model/, one can find the ecore model, genmodel and sample files. 

2. inside the ModellingAssignment1.test/scr/ one can finde StudyprogramValidatorTest file, for testing constraints 
