/**
 */
package studyprogram.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import studyprogram.SemesterCourse;
import studyprogram.StudyprogramFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester Course</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SemesterCourseTest extends TestCase {

	/**
	 * The fixture for this Semester Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterCourse fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterCourseTest.class);
	}

	/**
	 * Constructs a new Semester Course test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterCourseTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(SemesterCourse fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester Course test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterCourse getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StudyprogramFactory.eINSTANCE.createSemesterCourse());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SemesterCourseTest
