package studyprogram.tests;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import studyprogram.Course;
import studyprogram.Semester;
import studyprogram.SemesterCourse;
import studyprogram.StudyprogramFactory;

public class StudyprogramValidatorTest {

	@Before
	public void setUp() {
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());
	}

	@Test
	public void testCourse__Course_Code_Validation() {	
		Course c1 = StudyprogramFactory.eINSTANCE.createCourse();
		c1.setCode("IT3245");
		checkDiagnostic(c1, Diagnostic.OK);
		c1.setCode("TDT9876");
		checkDiagnostic(c1, Diagnostic.OK);
		
		c1.setCode("I1235");
		checkDiagnostic(c1, Diagnostic.ERROR);	
		c1.setCode("IT334");
		checkDiagnostic(c1, Diagnostic.ERROR);	
		c1.setCode("qw5123");
		checkDiagnostic(c1, Diagnostic.ERROR);		
	}
	
	private void checkDiagnostic(EObject root, int severity) {
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
		Assert.assertTrue(diagnostics.getSeverity() == severity);
	}
	
	private Semester allocateCourse(Course course, float credits) {
		SemesterCourse sc1 = StudyprogramFactory.eINSTANCE.createSemesterCourse();
		Semester s = StudyprogramFactory.eINSTANCE.createSemester();
		course.setCredits(credits);
		sc1.setCourse(course);
		s.getSemesterCourses().add(sc1);
		return s;
	}
	
	@Test
	public void testSemester__Semester_Credit_Validation() {	
		Course c = StudyprogramFactory.eINSTANCE.createCourse();	
	
		Semester s = allocateCourse(c, 7.5f);
		checkDiagnostic(s, Diagnostic.ERROR);
		Semester s2 = allocateCourse(c, 30f);
		checkDiagnostic(s2, Diagnostic.OK);
		
		
	}

}
