/**
 */
package studyprogram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link studyprogram.Semester#getSemesterNumber <em>Semester Number</em>}</li>
 *   <li>{@link studyprogram.Semester#getSemesterCourses <em>Semester Courses</em>}</li>
 * </ul>
 *
 * @see studyprogram.StudyprogramPackage#getSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='minCredits'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 minCredits='self.semesterCourses.course.credits-&gt;sum()&gt;=30.0'"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Semester Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Number</em>' attribute.
	 * @see #setSemesterNumber(String)
	 * @see studyprogram.StudyprogramPackage#getSemester_SemesterNumber()
	 * @model
	 * @generated
	 */
	String getSemesterNumber();

	/**
	 * Sets the value of the '{@link studyprogram.Semester#getSemesterNumber <em>Semester Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester Number</em>' attribute.
	 * @see #getSemesterNumber()
	 * @generated
	 */
	void setSemesterNumber(String value);

	/**
	 * Returns the value of the '<em><b>Semester Courses</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.SemesterCourse}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester Courses</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getSemester_SemesterCourses()
	 * @model containment="true"
	 * @generated
	 */
	EList<SemesterCourse> getSemesterCourses();

} // Semester
