/**
 */
package studyprogram;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link studyprogram.Program#getName <em>Name</em>}</li>
 *   <li>{@link studyprogram.Program#getYears <em>Years</em>}</li>
 *   <li>{@link studyprogram.Program#getSemesterList <em>Semester List</em>}</li>
 *   <li>{@link studyprogram.Program#getSpecialisations <em>Specialisations</em>}</li>
 *   <li>{@link studyprogram.Program#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see studyprogram.StudyprogramPackage#getProgram()
 * @model
 * @generated
 */
public interface Program extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see studyprogram.StudyprogramPackage#getProgram_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link studyprogram.Program#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Years</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Years</em>' attribute.
	 * @see #setYears(int)
	 * @see studyprogram.StudyprogramPackage#getProgram_Years()
	 * @model
	 * @generated
	 */
	int getYears();

	/**
	 * Sets the value of the '{@link studyprogram.Program#getYears <em>Years</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Years</em>' attribute.
	 * @see #getYears()
	 * @generated
	 */
	void setYears(int value);

	/**
	 * Returns the value of the '<em><b>Semester List</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Semester}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester List</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getProgram_SemesterList()
	 * @model containment="true"
	 * @generated
	 */
	EList<Semester> getSemesterList();

	/**
	 * Returns the value of the '<em><b>Specialisations</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Specialisation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialisations</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getProgram_Specialisations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Specialisation> getSpecialisations();

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link studyprogram.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see studyprogram.StudyprogramPackage#getProgram_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

} // Program
